package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.aliyuncs.exceptions.ClientException;
import com.itheima.constat.MessageConstant;
import com.itheima.constat.RedisConstant;
import com.itheima.entity.Result;
import com.itheima.pojo.Order;
import com.itheima.service.OrderService;
import com.itheima.utils.SMSUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/7
 * @description:
 * @version:1.0
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private JedisPool jedisPool;

    @Reference
    private OrderService orderService;


    @RequestMapping("submit")
    public Result submit(@RequestBody Map map){
        //从map中取出从前端页面发送来手机号与验证码
        String telephone = (String) map.get("telephone");
        String validateCode = (String) map.get("validateCode");
        //从redis内存中取出验证码
        Jedis jedis =jedisPool.getResource();
        String codeInRedis = jedis.get(telephone + RedisConstant.SENDTYPE_ORDER);
        jedis.close();

        if (codeInRedis == null || validateCode == null || !validateCode.equals(codeInRedis)){
            //验证码校验失败
            return new Result(false, MessageConstant.VALIDATECODE_ERROR);
        }
        //添加预约后的状态(到诊,未到诊)
        map.put("orderType", Order.ORDERTYPE_WEIXIN);
        Result result = null;
        try{
            result = orderService.order(map);
        }catch(Exception e){
            e.printStackTrace();
            return  new Result(false,MessageConstant.ORDER_FAIL);
        }

        if (result.isFlag()){
            //预约成功发送短信提示
            String orderDate = (String) map.get("orderDate");
            try {
                SMSUtils.sendShortMessage(SMSUtils.ORDER_NOTICE,telephone,orderDate);
            } catch (ClientException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    @RequestMapping("/findById")
    public Result findById(Integer id){
        try{

            Map map =orderService.findById(id);
            return new Result(true,MessageConstant.QUERY_ORDER_SUCCESS,map);
        }catch(Exception e){
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_ORDER_FAIL);
        }
    }

}











