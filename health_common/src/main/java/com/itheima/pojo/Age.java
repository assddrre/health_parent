package com.itheima.pojo;

import java.util.Date;

/**
 * @author ：Liu
 * @date ：Created in 2019/12/13
 * @description ：
 * @version: 1.0
 */
public class Age {
    private Date name;

    public Date getName() {
        return name;
    }

    public void setName(Date name) {
        this.name = name;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    private Integer value;

    @Override
    public String toString() {
        return "Age{" +
                "name=" + name +
                ", value=" + value +
                '}';
    }
}
