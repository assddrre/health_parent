package com.itheima.jobs;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.service.OrderSettingService;
import org.springframework.stereotype.Component;


/**
 * @authou: fu
 * @date:Created in 2019/12/13
 * @description:
 * @version:1.0
 */
@Component
public class ClearOrderSetting {

    @Reference
    private OrderSettingService orderSettingService;

    public void clear(){

        try {
            orderSettingService.deleteOrderSetting();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
