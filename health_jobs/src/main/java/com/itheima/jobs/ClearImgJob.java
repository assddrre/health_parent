package com.itheima.jobs;

import com.itheima.constat.RedisConstant;
import com.itheima.utils.QiniuUtils;
import org.springframework.beans.factory.annotation.Autowired;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import java.util.Set;

/**
 * @authou: fu
 * @date:Created in 2019/12/3
 * @description:
 * @version:1.0
 */
public class ClearImgJob {

    @Autowired
    private JedisPool jedisPool;

    public void clearImg() {
        Jedis jedis = jedisPool.getResource();
        Set<String> imgNames = jedis.sdiff(RedisConstant.SETMEAL_PIC_RESOURCES, RedisConstant.SETMEAL_PIC_DB_RESOURCES);
        if (imgNames!=null&&imgNames.size()>0){
            for (String imgName : imgNames) {
                QiniuUtils.deleteFileFromQiniu(imgName);
                System.out.println("定时任务,从七牛云删除图片"+imgName);
                jedis.srem(RedisConstant.SETMEAL_PIC_RESOURCES,imgName);
                System.out.println("定时任务,从redis删除图片"+imgName);
            }
        }
        jedis.close();
    }
}
