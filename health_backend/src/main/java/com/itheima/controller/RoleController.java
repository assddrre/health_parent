package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constat.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Reference
    private RoleService roleService;

    @RequestMapping("findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        return roleService.findPage(queryPageBean);
    }

    @RequestMapping("/add")
    public Result add(@RequestBody Map map){
        try{

            List<Integer> permissionIds = (List<Integer>) map.get("permissionIds");
            List<Integer> menuIds = (List<Integer>) map.get("menuIds");
            Map formData = (Map) map.get("formData");
            Role role = new Role();
            role.setName((String) formData.get("name"));
            role.setKeyword((String) formData.get("keyword"));
            role.setDescription((String) formData.get("description"));

            roleService.add(permissionIds,menuIds,role);

            return new Result(true,"新增角色成功");
        }catch(RuntimeException ex){
            return new Result(false, ex.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            return new Result(false, "新增角色失败");
        }
    }

    @RequestMapping("/findById")
    public Result findById(Integer id){
        try{
            Role role = roleService.findById(id);
            return new Result(true, "查询角色信息成功",role);
        }catch(Exception e){
            e.printStackTrace();
            return new Result(false, "查询角色信息成功");
        }
    }

    @RequestMapping("/findPermissionIdsByRoleId")
    public Result findPermissionIdsByRoleId(Integer id){
        try{
            List<Integer> list = roleService.findPermissionIdsByRoleId(id);
            return new Result(true,"查询权限信息成功",list);
        }catch(RuntimeException e){
            e.printStackTrace();
            return new Result(false,"查询权限信息失败");
        }
    }

    @RequestMapping("/findMenuIdsByRoleId")
    public Result findMenuIdsByRoleId(Integer id){
        try{
            List<Integer> list = roleService.findMenuIdsByRoleId(id);
            return new Result(true,"查询菜单信息成功",list);
        }catch(RuntimeException e){
            e.printStackTrace();
            return new Result(false,"查询菜单信息失败");
        }
    }

    @RequestMapping("/edit")
    public Result edit(@RequestBody Map map){
        try{

            List<Integer> permissionIds = (List<Integer>) map.get("permissionIds");
            List<Integer> menuIds = (List<Integer>) map.get("menuIds");
            Map formData = (Map) map.get("formData");
            Role role = new Role();
            role.setName((String) formData.get("name"));
            role.setKeyword((String) formData.get("keyword"));
            role.setDescription((String) formData.get("description"));
            role.setId((Integer) formData.get("id"));

            roleService.edit(permissionIds,menuIds,role);

            return new Result(true,"角色编辑成功");
        }catch(RuntimeException ex){
            return new Result(false, ex.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            return new Result(false, "角色编辑失败");
        }
    }
    @RequestMapping("/delete")
    public Result delete(Integer id){
        try{
            roleService.delete(id);
            return new Result(true, "删除角色成功");
        }catch (RuntimeException er){
            return new Result(false,er.getMessage());
        }catch(Exception e){
            e.printStackTrace();
            return new Result(false,"删除角色失败");
        }
    }

    @RequestMapping("/findAll")
    public Result findAll(){
        List<Role> roleList = roleService.findAll();
        return new Result(true, "获取权限成功",roleList);

    }
}
