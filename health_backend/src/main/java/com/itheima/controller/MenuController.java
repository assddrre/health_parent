package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constat.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Menu;
import com.itheima.service.MenuService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Reference
    private MenuService menuService;

    @RequestMapping("findAll")
    public Result findAll(){
        try{
            List<Menu> list = menuService.findAll();

            return new Result(true, "查询成功",list);
        }catch(Exception e){
            e.printStackTrace();
            return new Result(false, "查询成功");
        }
    }
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean){
        PageResult pageResult = menuService.findPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize(),queryPageBean.getQueryString());
        return pageResult;
    }
    //添加
    @RequestMapping("/add")
    public Result add(@RequestBody Menu menu){
        try {
            menuService.add(menu);

        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_MENU_FAIL);
        }
        return new Result(true,MessageConstant.ADD_MENU_SUCCESS);
    }
    @RequestMapping("/findById")
    public Result findById(Integer id){
        try {
            Menu menu = menuService.findById(id);
            return new Result(true,MessageConstant.QUERY_MENU_SUCCESS,menu);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.QUERY_MENU_FAIL);
        }
    }
    @RequestMapping("/edit")
    public Result edit(@RequestBody Menu menu){
        try {
            menuService.edit(menu);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.EDIT_MENU_FAIL);
        }
        return new Result(true,MessageConstant.EDIT_MENU_SUCCESS);
    }
    @RequestMapping("/deleteById")
    public Result deleteById(Integer id){
        try {
            menuService.deleteById(id);
        }catch (RuntimeException e){
            return new Result(false,e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false,MessageConstant.DELETE_MENU_FAIL);
        }
        return new Result(true,MessageConstant.DELETE_MENU_SUCCESS);
    }
    @RequestMapping("/findListAll")
    public Result findListAll(){
        List<Menu> menuList =menuService.findListAll();
        return new Result(true,MessageConstant.EDIT_MENU_SUCCESS,menuList);
    }
}
