package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constat.MessageConstant;
import com.itheima.entity.Result;
import com.itheima.service.Riliservice;
import com.itheima.utils.DateUtils;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author ：szy
 * @date ：Created in 2019/12/12
 * @description ：
 * @version: 1.0
 */
@RestController
@RequestMapping("/rili")
public class RiLiController {
    @Reference
    private Riliservice riliservice;

    @RequestMapping("/getShiJian")
    public Result getShiJian(@RequestBody Map map){
        String value1= (String) map.get("rili1");
        String value2= (String) map.get("rili2");

        System.out.println("Map集合打印开始日期"+value1);
        System.out.println("Map集合打印结束日期"+value2);

        Map data = new HashMap();
        List<String> months = new ArrayList<>();

        try {
            List<String> monthBetween = DateUtils.getMonthBetween(value1, value2, "yyyy-MM");
            for (String m :monthBetween){
                months.add(m);
            }

            data.put("months",months);

            List<Integer> memberCount = riliservice.findMemberCountByMonths(months);
            data.put("memberCount",memberCount);

            return new Result(true, MessageConstant.GET_MEMBER_NUMBER_REPORT_SUCCESS,data);
        } catch (Exception e) {
            e.printStackTrace();
           return new Result(false,MessageConstant.GET_MEMBER_NUMBER_REPORT_FAIL);
        }


    }
}
