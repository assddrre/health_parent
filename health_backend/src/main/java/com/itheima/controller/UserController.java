package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constat.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Menu;
import com.itheima.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/10
 * @description:
 * @version:1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {

    private String username;

    @Reference
    private UserService userService;

    @RequestMapping("/getUsername")
    public Result getUsername(){
        try{
            //获取权限控制上下文对象
            SecurityContext context = SecurityContextHolder.getContext();
            //获取当前经过身份验证的主体或身份验证请求签名
            Authentication authentication = context.getAuthentication();
            //获取接口UserDetails实现类对象User..
            org.springframework.security.core.userdetails.User user = (User) authentication.getPrincipal();
                                                                    //调方法getUsername 获取当前用户名名称.
            return new Result(true, MessageConstant.GET_USERNAME_SUCCESS,user.getUsername());
        }catch(RuntimeException e){
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_USERNAME_FAIL);
        }

    }
    @RequestMapping("/getMenuList")
    public Result getAllMenu() {
        try {
            SecurityContext context = SecurityContextHolder.getContext();
            Authentication authentication = context.getAuthentication();
            org.springframework.security.core.userdetails.User user = (User) authentication.getPrincipal();
            username = user.getUsername();
            List<Menu> parentMenu = userService.getParentMenu(username);
            return new Result(true, MessageConstant.GET_MENU_SUCCESS, parentMenu);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.GET_MENU_FAIL);
        }
    }
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {

        PageResult pageResult = userService.findPage(queryPageBean);

        return pageResult;
    }

    @RequestMapping("/add")
    public Result add(Integer[] roles, @RequestBody com.itheima.pojo.User user) {

        com.itheima.pojo.User userbyName = userService.findByUserName(user.getUsername());
        if (userbyName!=null){
            return new Result(false,"该用户已存在!");
        }

        try {
            if (user.getPassword() == null || user.getPassword() == "") {
                user.setPassword("1234");
            }
            userService.add(roles, user);
            return new Result(true, "添加用户成功");
        } catch (Exception e) {
            return new Result(false, "添加用户失败");
        }


    }

    @RequestMapping("/findById")
    public Result findById(Integer id) {
        com.itheima.pojo.User user = userService.findById(id);
        return new Result(true, "查询用户成功", user);
    }

    @RequestMapping("/findRoleIdByUserId")
    public Result findRoleIdByUserId(Integer id) {
        List<Integer> list = userService.findRoleIdByUserId(id);
        return new Result(true, "查询权限列表成功", list);
    }

    @RequestMapping("/edit")
    public Result edit(Integer[] roles, @RequestBody com.itheima.pojo.User user) {




        userService.edit(roles, user);
        return new Result(true, "修改成功");
    }

    @RequestMapping("/deleteById")
    public Result deleteById(Integer id) {

        try {
            com.itheima.pojo.User user = userService.findById(id);

            if ("adminn".equals(user.getUsername()) || "admin".equals(user.getUsername())) {
                return new Result(false, "该用户不可删除!");
            }

            userService.deleteById(id);
            return new Result(true, "删除成功");
        } catch (Exception e) {
            return new Result(false, "删除失败");
        }

    }

    @RequestMapping("/findByUsername")
    public Result findByUsername(String username) {
        com.itheima.pojo.User user = userService.findByUserName(username);
        if (user != null) {
            return new Result(false, "该用户名已被占用");
        }
        return new Result(true, "该用户名可以使用");
    }
    @RequestMapping("/EditCheckByUsername")
    public Result EditCheckByUsername(@RequestBody Map map){

        Integer id = (Integer) map.get("id");
        String username = (String) map.get("username");

        com.itheima.pojo.User userbyId = userService.findById(id);
        com.itheima.pojo.User userbyName = userService.findByUserName(username);

        //未修改名字
        if (userbyId.getUsername().equals(username)){
            return new Result(true,"用户名未修改");
        }else if(userbyName==null){
            return new Result(true,"该用户名可以使用");
        }else{
            return new Result(false,"该用户名已被占用");
        }

    }

}
