package com.itheima.controller;

import com.alibaba.dubbo.config.annotation.Reference;
import com.itheima.constat.MessageConstant;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Permission;
import com.itheima.service.PermissionService;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {

    @Reference
    private PermissionService permissionService;

    @RequestMapping("findAll")
    public Result findAll(){
        try{
            List<Permission> list = permissionService.findAll();

            return new Result(true, "查询成功",list);
        }catch(Exception e){
            e.printStackTrace();
            return new Result(false, "查询成功");
        }
    }

    //删除权限根据id
    @RequestMapping("/deletePermissionById")
    public Result deletePermissionById(Integer id) {
        try {
            permissionService.deletePermissionById(id);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new Result(false, e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.DELETE_PERMISSION_FAIL);
        }
        return new Result(true, MessageConstant.DELETE_PERMISSION_SUCCESS);
    }


    //编辑窗口 更新权限信息
    @RequestMapping("/editPermissionById")
    public Result editPermissionById(@RequestBody Permission permission) {
        try {
            permissionService.editPermissionById(permission);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.EDIT_PERMISSION_FAIL);
        }
        return new Result(true, MessageConstant.EDIT_PERMISSION_SUCCESS);
    }


    //编辑窗口 回显权限信息
    @RequestMapping("/findPermissionById")
    public Result findPermissionById(Integer id) {
        Permission permission = permissionService.findPermissionById(id);
        try {
            return new Result(true, MessageConstant.QUERY_PERMISSION_SUCCESS, permission);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.QUERY_PERMISSION_FAIL);
        }
    }

    //添加
    @RequestMapping("/add")
    public Result add(@RequestBody Permission permission) {
        try {
            permissionService.findAdd(permission);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ADD_PERMISSION_FAIL);
        }
        return new Result(true, MessageConstant.ADD_PERMISSION_SUCCESS);
    }

    //分页查询所有权限
    @RequestMapping("/findPage")
    public PageResult findPage(@RequestBody QueryPageBean queryPageBean) {
        PageResult pageResult = permissionService.findPage(queryPageBean);
        return pageResult;
    }
}
