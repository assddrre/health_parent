package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.MenuDao;
import com.itheima.dao.PermissionDao;
import com.itheima.dao.RoleDao;
import com.itheima.dao.UserDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Menu;
import com.itheima.pojo.Permission;
import com.itheima.pojo.Role;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @authou: fu
 * @date:Created in 2019/12/10
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PermissionDao permissionDao;




    @Override
    public User findByUserName(String username) {

        User user = userDao.findByUsreName(username);
        if (user == null){
            return null;
        }
        Set<Role> roles = roleDao.findByUserId(user.getId());
        if (roles !=null && roles.size()>0){
            for (Role role : roles) {

                Set<Permission> permissions = permissionDao.findByRoleId(role.getId());
                if (permissions!=null && permissions.size()>0){
                    role.setPermissions(permissions);
                }
            }
            user.setRoles(roles);
        }

        return user;
    }

    @Override
    public List<Menu> getParentMenu(String username) {
        //查询所有菜单
        List<Menu> allMenuList = userDao.findAllMenuList(username);

        //1级菜单
        List<Menu> parentMenuList = new ArrayList<>();

        for (Menu menu : allMenuList) {

            Integer parentMenuId = menu.getParentMenuId();
            if (parentMenuId == null) {
                parentMenuList.add(menu);
            }
        }

        //添加2级菜单
        for (Menu menu : parentMenuList) {
            Integer id = menu.getId();
            List<Menu> childrenMenuList = getChildrenMenu(id, allMenuList);
            menu.setChildren(childrenMenuList);

        }

        return parentMenuList;
    }

    //2级菜单查询
    public List<Menu> getChildrenMenu(Integer id,List<Menu> allMenuList) {
        List<Menu> childrenMenuList = new ArrayList<>();
        for (Menu menu : allMenuList) {
            Integer parentMenuId = menu.getParentMenuId();
            if (id==parentMenuId){
                childrenMenuList.add(menu);

            }

        }

        return  childrenMenuList;
    }

    @Override
    public PageResult findPage(QueryPageBean queryPageBean)  {
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        Page<User> users = userDao.selectByCondition(queryPageBean.getQueryString());

        return new PageResult(users.getTotal(),users.getResult());
    }

    @Override
    public void add(Integer[] roles, User user) {
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String password = user.getPassword();
        password = encoder.encode(password);
        user.setPassword(password);
        userDao.add(user);
        setUserAndRole(roles,user.getId());
    }

    @Override
    public User findById(Integer id) {

        User user = userDao.findById(id);

        return user;
    }

    @Override
    public List<Integer> findRoleIdByUserId(Integer id) {

        return userDao.findRoleIdByUserId(id);
    }

    @Override
    public void edit(Integer[] roles, User user) {
        if(user.getPassword()!=null) {
            BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
            String password = user.getPassword();
            password = encoder.encode(password);
            user.setPassword(password);
        }
        userDao.deleteAssociation(user.getId());
        setUserAndRole(roles,user.getId());
        userDao.edit(user);
    }

    @Override
    public void deleteById(Integer id) {

        try {
            userDao.deleteAssociation(id);
        }catch (Exception e){
            e.printStackTrace();
        }

        userDao.deleteById(id);
    }

    public void setUserAndRole(Integer[] roles, Integer userId){
        if (roles != null && roles.length > 0){
            for (Integer role:roles){
                Map map = new HashMap();
                map.put("userId",userId);
                map.put("roleId",role);
                userDao.setUserAndRole(map);
            }
        }
    }
}
