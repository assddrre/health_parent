package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constat.MessageConstant;
import com.itheima.dao.CheckGroupDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.service.CheckGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/1
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class CheckGroupServiceImpl implements CheckGroupService {

    @Autowired
    private CheckGroupDao checkGroupDao;

    //添加检查组
    public void add(Integer[] checkitemIds, CheckGroup checkGroup) {
        checkGroupDao.add(checkGroup);
        setCheckGroupAndCheckItem(checkitemIds,checkGroup.getId());
    }

    //分页查询
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        Page<CheckGroup> page = checkGroupDao.selectByCondition(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(),page.getResult());
    }

    //通过ID查询 回显
    public CheckGroup findById(Integer checkGroupId) {
        return checkGroupDao.findById(checkGroupId);
    }
    //查询检查项与检查组关系表  回显
    public List<Integer> findCheckItemIdsByCheckGroupId(Integer id) {
            return checkGroupDao.findCheckItemIdsByCheckGroupId(id);
    }

    //编辑检查组
    public void edit(Integer[] checkitemIds, CheckGroup checkGroup) {
        //清理当前关联表关系
        checkGroupDao.deleteAssociation(checkGroup.getId());
        //重新建立新的关联关系
        setCheckGroupAndCheckItem(checkitemIds,checkGroup.getId());
        //修改检查组信息
        checkGroupDao.update(checkGroup);
    }

    //删除
    public void delete(Integer id) {
       long count = checkGroupDao.findCountByCheckGroupId(id);
        if (count > 0 ){
            throw new RuntimeException(MessageConstant.CHECKITEMHASASSOCIATION);
        }

       long checkItemCount =checkGroupDao.findCountFormCheckItemByCheckGroupId(id);
        if (checkItemCount > 0){
            //删除检查组与检查项关联关系中间表
            checkGroupDao.deleteAssociation(id);
        }
        //删除检查组信息
        checkGroupDao.delete(id);
    }

    //查询检查组所有数据
    public List<CheckGroup> findAll() {
        return checkGroupDao.findAll();
    }

    //设置关联关系
    public  void  setCheckGroupAndCheckItem(Integer[] checkitemIds,Integer checkGroupId){

        if (checkitemIds!=null&&checkitemIds.length>0){
            for (Integer checkitemId : checkitemIds) {
                Map map = new HashMap();
                map.put("checkItemId",checkitemId);
                map.put("checkGroupId",checkGroupId);
                checkGroupDao.setCheckGroupAndCheckItem(map);
            }
        }
    }
}
