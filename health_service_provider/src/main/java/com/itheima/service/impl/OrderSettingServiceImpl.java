package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.OrderSettingDao;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;
import com.itheima.utils.DateUtils;
import com.itheima.utils.POIUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @authou: fu
 * @date:Created in 2019/12/4
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class OrderSettingServiceImpl implements OrderSettingService {

    @Autowired
    private OrderSettingDao orderSettingDao;

    public void add(List<OrderSetting> list) {
        if (list != null && list.size() > 0) {
            for (OrderSetting orderSetting : list) {
                //根据时间查询是够进行了预约设置
               long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());

               if (count>0){
                   //已经做了预约设置,进行更新
                   orderSettingDao.editNumberByOrderDate(orderSetting);
               }else {
                   orderSettingDao.add(orderSetting);
               }
            }
        }
    }

    @Override
    public List<Map> getOrderSettingByMonth(String date) {

        String begin = date+"-1";
        String end = date+"-31";
        Map map = new HashMap();
        map.put("dateBegin",begin);
        map.put("dateEnd",end);

        List<OrderSetting> orderSettingList = orderSettingDao.getOrderSettingByMonth(map);
        List<Map> data = new ArrayList<>();

        if (orderSettingList!=null && orderSettingList.size()>0){
            for (OrderSetting orderSetting : orderSettingList) {
                Map map1 = new HashMap();
                map1.put("date",orderSetting.getOrderDate().getDate());
                map1.put("number",orderSetting.getNumber());
                map1.put("reservations",orderSetting.getReservations());
                data.add(map1);
            }
        }
        return data;
    }

    @Override
    public void editNumberByDate(OrderSetting orderSetting) {
        //判断是否进行过设置
        long count = orderSettingDao.findCountByOrderDate(orderSetting.getOrderDate());
        if (count > 0){
            //已经设置过了，执行更新操作
            orderSettingDao.editNumberByOrderDate(orderSetting);
        }else {
            //没有进行设置，执行插入操作
            orderSettingDao.add(orderSetting);
        }
    }

    public void deleteOrderSetting() throws Exception {
        //获取本月第一天
        String firstDay4ThisMonth = DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());
        orderSettingDao.delete(firstDay4ThisMonth);

    }


}
