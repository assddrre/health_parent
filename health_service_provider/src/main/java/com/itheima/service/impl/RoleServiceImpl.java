package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.dao.RoleDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Role;
import com.itheima.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleDao roleDao;

    @Override
    public PageResult findPage(QueryPageBean queryPageBean) {

        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        Page<Role> roles = roleDao.selectByCondition(queryPageBean.getQueryString());
        return new PageResult(roles.getTotal(),roles.getResult());
    }

    public void add(List<Integer> permissionIds, List<Integer> menuIds, Role role) {

        if ("系统管理员".equals(role.getName()) || "ROLE_ADMIN".equals(role.getKeyword())){
            throw new RuntimeException("不能新增角色名称为系统管理员的角色名称,或者不能新增ROLE_ADMIN的角色权限码");
        }

        roleDao.add(role);
        setCheckGroupAndCheckItem(permissionIds,menuIds,role.getId());
    }


    public Role findById(Integer id) {
        return roleDao.findById(id);
    }

    public List<Integer> findPermissionIdsByRoleId(Integer id) {
        return roleDao.findPermissionIdsByRoleId(id);
    }

    @Override
    public List<Integer> findMenuIdsByRoleId(Integer id) {

        return roleDao.findMenuIdsByRoleId(id);
    }

    @Override
    public void edit(List<Integer> permissionIds, List<Integer> menuIds, Role role) {


        roleDao.deletePermissionAssociation(role.getId());
        roleDao.deleteMenuAssociation(role.getId());
        setCheckGroupAndCheckItem(permissionIds,menuIds,role.getId());

        roleDao.update(role);
    }

    //设置关联关系
    public  void  setCheckGroupAndCheckItem(List<Integer> permissionIds,List<Integer> menuIds,Integer roleId){

        if (permissionIds!=null&&permissionIds.size()>0){
            for (Integer permissionId : permissionIds) {
                Map map = new HashMap();
                map.put("roleId",roleId);
                map.put("permissionId",permissionId);
                roleDao.setRoleAndPermission(map);
            }
        }
        if (menuIds!=null&&menuIds.size()>0){
            for (Integer menuId : menuIds) {
                Map map1 = new HashMap();
                map1.put("roleId",roleId);
                map1.put("menuId",menuId);
                roleDao.setRoleAndMenu(map1);
            }
        }
    }
    public void delete(Integer id) {

        Role role = roleDao.findById(id);
        if ("系统管理员".equals(role.getName()) || "ROLE_ADMIN".equals(role.getKeyword())){
            throw new RuntimeException("此角色为系统管理员,不能进行删除");
        }

        long userCount = roleDao.findCountUserByRoleId(id);
        if (userCount>0){
            throw new RuntimeException("当前角色已跟用户关联,不能删除");
        }
        long menuCount = roleDao.findCountMenuByRoleId(id);
        if (menuCount>0){
            roleDao.deleteMenu(id);
        }
        long permissionCount = roleDao.findCountPermissionByRoleId(id);
        if (permissionCount>0){
            roleDao.deletePermission(id);
        }

        roleDao.deleteRole(id);
    }

    @Override
    public List<Role> findAll() {

        return roleDao.findAll();

    }
}
