package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.constat.MessageConstant;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.dao.OrderSettingDao;
import com.itheima.entity.Result;
import com.itheima.pojo.Member;
import com.itheima.pojo.Order;
import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderService;
import com.itheima.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import sun.dc.pr.PRError;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/7
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderSettingDao orderSettingDao;
    @Autowired
    private MemberDao memberDao;
    @Autowired
    private OrderDao orderDao;


    public Result order(Map map) {

        String orderDate = (String) map.get("orderDate");
        Date date = null;
        try {
            date = DateUtils.parseString2Date(orderDate);
        } catch (Exception e) {
            e.printStackTrace();
            return new Result(false, MessageConstant.ORDER_FAIL);
        }

        //检查用户所选择的项目预约日期是否已经提前进行了预约设置,如果没有设置则无法预约.
        OrderSetting orderSetting = orderSettingDao.findByOrderDate(date);
        if (orderSetting == null) {
            return new Result(false, MessageConstant.SELECTED_DATE_CANNOT_ORDER);
        }
        //检查所选项目日期是否已经预约满了,如果预约满了就不能进行预约.
        int number = orderSetting.getNumber();
        int reservations = orderSetting.getReservations();
        if (reservations >= number) {
            return new Result(false, MessageConstant.ORDER_FULL);
        }


        //检查当前用户是否已经是会员,如果是则直接预约成功,如果不是自动完成注册并进行预约.
        String telephone = (String) map.get("telephone");
        Member member = memberDao.findByTelephone(telephone);
        if (member == null) {
            //不是会员,自动注册
            member = new Member();
            member.setIdCard((String) map.get("idCard"));
            member.setName((String) map.get("name"));
            member.setPhoneNumber(telephone);
            member.setSex((String) map.get("sex"));
            member.setRegTime(new Date());

            memberDao.add(member);
        } else {
            //检查用户是否重复预约(同一个用户同一天预约同一个套餐),如果重复预约则无法再次预约
            Integer memberId = member.getId();
            int setmealId = Integer.parseInt((String) map.get("setmealId"));

            Order order = new Order();
            order.setMemberId(memberId);
            order.setSetmealId(setmealId);
            order.setOrderDate(date);
            List<Order> orderList = orderDao.findByCondition(order);
            if (orderList != null && orderList.size() > 0) {
                //重复预约
                return new Result(false, MessageConstant.HAS_ORDERED);
            }
        }
        //更新已预约人数
        orderSetting.setReservations(orderSetting.getReservations() + 1);
        orderSettingDao.editReservationsByOrderDate(orderSetting);
        //保存预约信息
        Order order1 = new Order(member.getId(),
                date,
                (String) map.get("orderType"),
                Order.ORDERSTATUS_NO,
                Integer.parseInt((String) map.get("setmealId")));
        orderDao.add(order1);
        return new Result(true,MessageConstant.ORDER_SUCCESS,order1.getId());

    }

    @Override
    public Map findById(Integer id) {
         Map map = orderDao.findById(id);
        Date orderDate = (Date)map.get("orderDate");

        try {
            //覆盖原来存储的时间数据格式
            map.put("orderDate",DateUtils.parseDate2String(orderDate));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }
}










