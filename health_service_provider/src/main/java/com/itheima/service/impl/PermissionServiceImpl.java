package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constat.MessageConstant;
import com.itheima.dao.PermissionDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Permission;
import com.itheima.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class PermissionServiceImpl implements PermissionService {

    @Autowired
    private PermissionDao permissionDao;

    @Override
    public List<Permission> findAll() {

        return permissionDao.findAll();
    }

    //分页查询所有权限
    public PageResult findPage(QueryPageBean queryPageBean) {
        PageHelper.startPage(queryPageBean.getCurrentPage(), queryPageBean.getPageSize());
        Page<Permission> page = permissionDao.selectPageByCondition(queryPageBean.getQueryString());
        return new PageResult(page.getTotal(), page.getResult());
    }

    //添加
    public void findAdd(Permission permission) {
        permissionDao.findAdd(permission);
    }

    //编辑窗口 回显权限信息
    public Permission findPermissionById(Integer id) {
        return permissionDao.findPermissionById(id);
    }

    //编辑窗口 更新权限信息
    public void editPermissionById(Permission permission) {
        permissionDao.editPermissionById(permission);
    }

    //删除权限根据id
    public void deletePermissionById(Integer id) {
        long count= permissionDao.selectCountByPermissionId(id);
        if (count>0){
            //已关联其角色信息
            throw new RuntimeException(MessageConstant.PERMISSIONHASASSOCIATION);
        }
        permissionDao.deletePermissionById(id);
    }
}
