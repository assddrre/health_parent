package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.RiLiDao;
import com.itheima.service.Riliservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：szy
 * @date ：Created in 2019/12/13
 * @description ：
 * @version: 1.0
 */
@Service
@Transactional
public class RiLiServiceImpl implements Riliservice {
    @Autowired
    private RiLiDao riLiDao;
    //测试 : 3;
    @Override
    public List<Integer> findMemberCountByMonths(List<String> months) {
        List<Integer> list = new ArrayList<>();
        if(months!=null&&months.size()>0){
            for (String month : months) {
                Integer count = riLiDao.findMemberCountBeforeDate(month);
                list.add(count);
            }
        }
        return list;
    }
}
