package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.itheima.constat.MessageConstant;
import com.itheima.dao.CheckItemDao;
import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckItem;
import com.itheima.service.CheckItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/11/29
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class CheckItemServiceImpl implements CheckItemService {

    @Autowired
    private CheckItemDao checkItemDao;

    //新增
    public void add(CheckItem checkItem) {
        checkItemDao.add(checkItem);
    }

    //分页查询
    public PageResult findPage(QueryPageBean queryPageBean) {
        //开始分页  设置分页条件
        PageHelper.startPage(queryPageBean.getCurrentPage(),queryPageBean.getPageSize());
        Page<CheckItem> page = checkItemDao.selectByCondition(queryPageBean.getQueryString());

        return new PageResult(page.getTotal(),page.getResult());
    }

    //根据id删除检查项，业务规则：如果当前检查项已经被关联到某个检查组，则不能删除
    public void deleteById(Integer id) {
        //查询当前检查项是否已经关联到检查组
        long count = checkItemDao.findCountByCheckItemId(id);
        if(count > 0){
            //已经被关联，不能删除
            throw new RuntimeException(MessageConstant.CHECKITEMHASASSOCIATION);
        }
        //未被关联，可以删除
        checkItemDao.deleteById(id);
    }

    //回显 通过Id查询
    public CheckItem findById(Integer id) {
       return checkItemDao.findById(id);

    }

    //修改
    public void update(CheckItem checkItem) {
         checkItemDao.update(checkItem);
    }

    public List<CheckItem> findAll() {
        return  checkItemDao.findAll();

    }
}
