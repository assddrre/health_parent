package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MemberDao;
import com.itheima.pojo.Age;
import com.itheima.pojo.Member;
import com.itheima.service.MemberService;
import com.itheima.utils.DateUtils;
import com.itheima.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @authou: fu
 * @date:Created in 2019/12/8
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class MemberServiceImpl implements MemberService {

    @Autowired
    private MemberDao memberDao;

    @Override
    public Member findByTelephone(String telephone) {

        return memberDao.findByTelephone(telephone);
    }

    @Override
    public void add(Member member) {
        if (member.getPassword()!=null){
            member.setPassword(MD5Utils.md5(member.getPassword()));
        }
        memberDao.add(member);
    }

    @Override
    public List<Integer> findMemberCountByMonths(List<String> months) {
        List<Integer> memberCount = new ArrayList<>();
        if (months != null && months.size()>0){
            for (String month : months) {

                Integer count = memberDao.findMemberCountByMonths(month + ".31");
                memberCount.add(count);
            }
        }
        return memberCount;
    }

    //饼状图性别
    public List<Map<String,Object>> findMemberBySex(){
        List<Map<String,Object>> list1 = memberDao.findMemberBySex();
        List<Map<String, Object>> list = deleteNull(list1);
        for (Map<String, Object> map : list) {
            String name = (String) map.get("name");
            if (name.equals("1")) {
                map.put("name", '男');
            } else if (name.equals("2")){
                map.put("name", '女');
            }
        }

        return list;
    }

    //饼状图年龄
    public List<Map<String,Object>> findMemberByAge()  {
        List<com.itheima.pojo.Age> list1 = memberDao.findMemberByBirthday();
        List<Age> list = deleteNullAge(list1);
        Map<String, Object> less18 = new HashMap();
        less18.put("name", "0-18");
        less18.put("value", 0);
        Map<String, Object> less30 = new HashMap<>();
        less30.put("name", "18-30");
        less30.put("value", 0);
        Map<String, Object> less45 = new HashMap<>();
        less45.put("name", "30-45");
        less45.put("value", 0);
        Map<String, Object> greater45 = new HashMap<>();
        greater45.put("name", "45以上");
        greater45.put("value", 0);

        if (list.size()>0 && list!= null){
            for (com.itheima.pojo.Age age : list) {
                Date name = age.getName();
                Integer age1 = DateUtils.countAgeByBirthday(name);
                Integer value = age.getValue();
                if (age1 >= 0 && age1 <= 18) {
                    Integer value18 = (Integer) less18.get("value");
                    value18 += value;
                    less18.put("value", value18);
                } else if (age1 > 18 && age1 <= 30) {
                    Integer value30 = (Integer) less30.get("value");
                    value30 += value;
                    less30.put("value", value30);
                } else if (age1 > 30 && age1 <= 45) {
                    Integer value45 = (Integer) less45.get("value");
                    value45 += value;
                    less45.put("value", value45);
                } else {
                    Integer value_45 = (Integer) greater45.get("value");
                    value_45 += value;
                    greater45.put("value", value_45);
                }
            }
        }
        List<Map<String, Object>> data = new ArrayList<>();
        data.add(less18);
        data.add(less30);
        data.add(less45);
        data.add(greater45);
        return data;
    }

    public List<Map<String,Object>> deleteNull( List<Map<String,Object>> list){
        for (int i = 0;i<list.size();i++){
            Map<String,Object> map = list.get(i);
            if (map.get("name")==null){
                list.remove(i);
            }
        }
        return list;
    }
    public List<Age> deleteNullAge(List<Age> list){
        for (int i = 0;i<list.size();i++){
            Age age = list.get(i);
            if (age.getName()==null){
                list.remove(i);
            }
        }
        return list;
    }
}
