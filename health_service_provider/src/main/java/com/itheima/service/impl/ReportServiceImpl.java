package com.itheima.service.impl;

import com.alibaba.dubbo.config.annotation.Service;
import com.itheima.dao.MemberDao;
import com.itheima.dao.OrderDao;
import com.itheima.service.ReportService;
import com.itheima.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/11
 * @description:
 * @version:1.0
 */
@Service
@Transactional
public class ReportServiceImpl implements ReportService {

    @Autowired
    private MemberDao memberDao;
    @Autowired
    private OrderDao orderDao;

    @Override
    public Map<String, Object> findReport() throws Exception {

        Map<String,Object> map = new HashMap<>();
        //当前日期
        String reportDate = DateUtils.parseDate2String(new Date());
        //获取本周一的日期
        String thisWeekMonday = DateUtils.parseDate2String(DateUtils.getThisWeekMonday());
        //获取本月第一天
        String firstDay4ThisMonth = DateUtils.parseDate2String(DateUtils.getFirstDay4ThisMonth());


        //获取今日新增会员数
        Integer todayNewMember = memberDao.findByTodayNewMember(reportDate);
        //获取总会员数
        Integer totalMember = memberDao.findByCountMember();
        //获取本周新增会员数
        Integer thisWeekNewMember = memberDao.findByThisWeekMonday(thisWeekMonday);
        //获取本月新增会员数
        Integer thisMonthNewMember = memberDao.findByThisMonth(firstDay4ThisMonth);


        //获取今日预约数
        Integer todayOrderNumber = orderDao.findByToday(reportDate);
        //获取今日到诊数
        Integer todayVisitsNumber = orderDao.findByTodayVisits(reportDate);
        //获取本周预约数
        Integer thisWeekOrderNumber = orderDao.findByWeekOrder(thisWeekMonday);
        //获取本周到诊数
        Integer thisWeekVisitsNumber = orderDao.findByWeekVisits(thisWeekMonday);
        //获取本月预约数
        Integer thisMonthOrderNumber = orderDao.findByMonthOrder(firstDay4ThisMonth);
        //获取本月到诊数
        Integer thisMonthVisitsNumber = orderDao.findByMonthVisits(firstDay4ThisMonth);


        //热门套餐
        List<Map> hotSetmeal = orderDao.findHotSetmeal();


        map.put("reportDate",reportDate);

        map.put("todayNewMember",todayNewMember);
        map.put("totalMember",totalMember);
        map.put("thisWeekNewMember",thisWeekNewMember);
        map.put("thisMonthNewMember",thisMonthNewMember);

        map.put("todayOrderNumber",todayOrderNumber);
        map.put("todayVisitsNumber",todayVisitsNumber);
        map.put("thisWeekOrderNumber",thisWeekOrderNumber);
        map.put("thisWeekVisitsNumber",thisWeekVisitsNumber);
        map.put("thisMonthOrderNumber",thisMonthOrderNumber);
        map.put("thisMonthVisitsNumber",thisMonthVisitsNumber);

        map.put("hotSetmeal",hotSetmeal);

        return map;
    }
}
