package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Menu;
import com.itheima.pojo.User;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/10
 * @description:
 * @version:1.0
 */
public interface UserDao {

    User findByUsreName(String username);

    List<Menu> findAllMenuList(String username);

    Page<User> selectByCondition(String queryString);

    void setUserAndRole(Map map);

    void add(User user);

    User findById(Integer id);

    List<Integer> findRoleIdByUserId(Integer id);

    void deleteAssociation(Integer id);

    void edit(User user);

    void deleteById(Integer id);
}
