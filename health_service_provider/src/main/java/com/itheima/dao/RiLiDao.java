package com.itheima.dao;

import java.util.List;
import java.util.Map;

/**
 * @author ：szy
 * @date ：Created in 2019/12/13
 * @description ：
 * @version: 1.0
 */
public interface RiLiDao {


    Integer findMemberCountBeforeDate(String date);
}
