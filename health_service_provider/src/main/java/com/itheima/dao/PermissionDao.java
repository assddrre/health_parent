package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Permission;

import java.util.List;
import java.util.Set;

/**
 * @authou: fu
 * @date:Created in 2019/12/10
 * @description:
 * @version:1.0
 */
public interface PermissionDao {
    Set<Permission> findByRoleId(Integer id);

    List<Permission> findAll();

    //分页查询所有权限
    Page<Permission> selectPageByCondition(String queryString);
    //添加
    void findAdd(Permission permission);
    //编辑窗口 回显权限信息
    Permission findPermissionById(Integer id);
    //编辑窗口 更新权限信息
    void editPermissionById(Permission permission);
    //删除权限 前先查询有无关联关系
    long selectCountByPermissionId(Integer id);
    //删除权限根据id
    void deletePermissionById(Integer id);

}
