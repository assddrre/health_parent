package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.entity.Result;
import com.itheima.pojo.CheckGroup;
import com.itheima.pojo.Role;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @authou: fu
 * @date:Created in 2019/12/10
 * @description:
 * @version:1.0
 */
public interface RoleDao {
    Set<Role> findByUserId(Integer id);

    Page<Role> selectByCondition(String queryString);

    void add(Role role);

    void setRoleAndPermission(Map map);

    void setRoleAndMenu(Map map1);

    Role findById(Integer id);

    List<Integer> findPermissionIdsByRoleId(Integer id);

    List<Integer> findMenuIdsByRoleId(Integer id);

    void deletePermissionAssociation(Integer id);

    void deleteMenuAssociation(Integer id);

    void update(Role role);
    long findCountUserByRoleId(Integer id);

    long findCountMenuByRoleId(Integer id);

    long findCountPermissionByRoleId(Integer id);

    void deletePermission(Integer id);

    void deleteRole(Integer id);

    void deleteMenu(Integer id);

    List<Role> findAll();
}
