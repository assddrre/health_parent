package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.Menu;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
public interface MenuDao {
    List<Menu> findAll();
    Page<Menu> findPage(String queryString);
    void add(Menu menu);
    Menu findById(Integer id);
    void edit(Menu menu);
    long findCountByMenuId(Integer id);
    void deleteById(Integer id);
    List<Menu> findListAll();
    long findCountByParentMenuId(Integer id);
    Menu findParentMenuIdById(Integer parentMenuId);

}
