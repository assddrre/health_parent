package com.itheima.dao;

import com.itheima.pojo.Age;
import com.itheima.pojo.Member;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/7
 * @description:
 * @version:1.0
 */
public interface MemberDao {
    Member findByTelephone(String telephone);
    void add(Member member);

    Integer findMemberCountByMonths(String month);

    Integer findByThisWeekMonday(String thisWeekMonday);

    Integer findByTodayNewMember(String reportDate);

    Integer findByCountMember();

    Integer findByThisMonth(String firstDay4ThisMonth);

    public List<Map<String,Object>> findMemberBySex();

    public List<Age> findMemberByBirthday();
}
