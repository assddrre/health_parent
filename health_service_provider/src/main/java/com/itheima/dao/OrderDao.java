package com.itheima.dao;

import com.itheima.pojo.Order;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/7
 * @description:
 * @version:1.0
 */
public interface OrderDao {
    List<Order> findByCondition(Order order);

    void add(Order order1);

    Map findById(Integer id);

    Integer findByToday(String reportDate);

    Integer findByTodayVisits(String reportDate);

    Integer findByWeekOrder(String thisWeekMonday);

    Integer findByWeekVisits(String thisWeekMonday);

    Integer findByMonthOrder(String firstDay4ThisMonth);

    Integer findByMonthVisits(String firstDay4ThisMonth);

    List<Map> findHotSetmeal();
}
