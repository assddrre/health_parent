package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckGroup;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/1
 * @description:
 * @version:1.0
 */
public interface CheckGroupDao {
    void add(CheckGroup checkGroup);
    void setCheckGroupAndCheckItem(Map map);
    Page<CheckGroup> selectByCondition(String queryString);
    CheckGroup findById(Integer checkGroupId);
    List<Integer> findCheckItemIdsByCheckGroupId(Integer id);
    void deleteAssociation(Integer checkGroup);
    void update(CheckGroup checkGroup);
    long findCountByCheckGroupId(Integer id);
    void delete(Integer id);
    long findCountFormCheckItemByCheckGroupId(Integer id);
    List<CheckGroup> findAll();
}
