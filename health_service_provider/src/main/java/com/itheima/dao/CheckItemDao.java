package com.itheima.dao;

import com.github.pagehelper.Page;
import com.itheima.pojo.CheckItem;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/11/29
 * @description:
 * @version:1.0
 */
public interface CheckItemDao {
    public void add(CheckItem checkItem);
    public Page<CheckItem> selectByCondition(String queryString);
    public long findCountByCheckItemId(Integer checkItemId);
    public void deleteById(Integer id);
    public CheckItem findById(Integer id);
    public void update(CheckItem checkItem);
    public List<CheckItem> findAll();
}
