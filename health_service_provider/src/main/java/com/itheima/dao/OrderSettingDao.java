package com.itheima.dao;

import com.itheima.pojo.OrderSetting;
import com.itheima.service.OrderSettingService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/4
 * @description:
 * @version:1.0
 */
public interface OrderSettingDao {
    long findCountByOrderDate(Date orderDate);

    void editNumberByOrderDate(OrderSetting orderSetting);

    void add(OrderSetting orderSetting);

    List<OrderSetting> getOrderSettingByMonth(Map map);

    OrderSetting findByOrderDate(Date date);

    void editReservationsByOrderDate(OrderSetting orderSetting);

    void delete(String firstDay4ThisMonth);
}
