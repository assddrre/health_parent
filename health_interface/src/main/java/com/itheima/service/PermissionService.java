package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Permission;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
public interface PermissionService {

    List<Permission> findAll();
    //分页查询所有权限
    PageResult findPage(QueryPageBean queryPageBean);
    //添加
    void findAdd(Permission permission);
    //编辑窗口 回显权限信息
    Permission findPermissionById(Integer id);
    //编辑窗口 更新权限信息
    void editPermissionById(Permission permission);
    //删除权限根据id
    void deletePermissionById(Integer id);
}
