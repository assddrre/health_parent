package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.Menu;
import com.itheima.pojo.User;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/12/10
 * @description:
 * @version:1.0
 */
public interface UserService {
    User findByUserName(String username);

    List<Menu> getParentMenu(String username);

    PageResult findPage(QueryPageBean queryPageBean);

    void add(Integer[] checkitemIds, User user);

    User findById(Integer id);

    List<Integer> findRoleIdByUserId(Integer id);

    void edit(Integer[] roles, User user);

    void deleteById(Integer id);
}
