package com.itheima.service;

import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/11
 * @description:
 * @version:1.0
 */
public interface ReportService {
    Map<String, Object> findReport() throws Exception;
}
