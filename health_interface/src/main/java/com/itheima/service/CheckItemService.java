package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.pojo.CheckItem;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/11/29
 * @description:
 * @version:1.0
 */
public interface CheckItemService {

    public void add(CheckItem checkItem);

    public PageResult findPage(QueryPageBean queryPageBean);

    public void deleteById(Integer id);

    public CheckItem findById(Integer id);

    public void update(CheckItem checkItem);

    public List<CheckItem> findAll();
}
