package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.pojo.Menu;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
public interface MenuService {

    List<Menu> findAll();
    PageResult findPage(Integer currentPage, Integer pageSize, String queryString);
    void add(Menu menu);
    Menu findById(Integer id);
    void edit(Menu menu);
    void deleteById(Integer id);
    List<Menu> findListAll();
}
