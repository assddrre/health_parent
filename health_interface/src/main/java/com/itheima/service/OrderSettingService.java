package com.itheima.service;

import com.itheima.pojo.OrderSetting;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/4
 * @description:
 * @version:1.0
 */
public interface OrderSettingService {
    void add(List<OrderSetting> list);

    List<Map> getOrderSettingByMonth(String date);

    void editNumberByDate(OrderSetting orderSetting);

    void deleteOrderSetting() throws Exception;

}
