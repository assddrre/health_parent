package com.itheima.service;

import com.itheima.pojo.Member;

import java.util.List;
import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/8
 * @description:
 * @version:1.0
 */
public interface MemberService {
    public Member findByTelephone(String telephone);

    void add(Member member);

    List<Integer> findMemberCountByMonths(List<String> months);

    public List<Map<String,Object>> findMemberBySex();

    public List<Map<String,Object>> findMemberByAge() throws Exception;
}
