package com.itheima.service;

import com.itheima.entity.PageResult;
import com.itheima.entity.QueryPageBean;
import com.itheima.entity.Result;
import com.itheima.pojo.Role;

import java.util.List;

/**
 * @authou: fu
 * @date:Created in 2019/12/12
 * @description:
 * @version:1.0
 */
public interface RoleService {
    PageResult findPage(QueryPageBean queryPageBean);

    void add(List<Integer> permissionIds, List<Integer> menuIds, Role role);

    Role findById(Integer id);

    List<Integer> findPermissionIdsByRoleId(Integer id);

    List<Integer> findMenuIdsByRoleId(Integer id);

    void edit(List<Integer> permissionIds, List<Integer> menuIds, Role role);

    void delete(Integer id);

    List<Role> findAll();
}
