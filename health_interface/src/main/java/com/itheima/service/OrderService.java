package com.itheima.service;

import com.itheima.entity.Result;

import java.util.Map;

/**
 * @authou: fu
 * @date:Created in 2019/12/7
 * @description:
 * @version:1.0
 */
public interface OrderService {
    Result order(Map map);

    Map findById(Integer id);
}
